import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  files: ['__tests__/**/*.spec.js'],
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: './setupTest.js',
    coverage: {
      reportsDirectory: './tests/unit/coverage'
    }
  },
  transpile: [
    'react',
    'react-dom', // Add any other dependencies that use JSX
  ],

})
