import { render, test, expect,beforeAll,beforeEach } from 'vitest';
import { Title } from '../../../src/components'; 
import "@testing-library/jest-dom";

test('Title component must be rendered with correct properties',() => {
  const title = 'About me';

  beforeAll(()=> render(<Title title={title} />));

  beforeEach(()=> expect(screen.findByText({title})).toBeInTheDocument());
});
