import { render, test,beforeAll,beforeEach,expect } from 'vitest';
import { Address } from '../../../src/components';
import "@testing-library/jest-dom";
test('Address Component must be rendered with correct properties', () => {
  const toLink = "mailto:eshqinferzeliyev@gmail.com";
  const icon = "GrMail"; 
  const text = "eshqinferzeliyev@gmail.com";

   beforeAll(()=> render(<Address toLink={toLink} icon={icon} text={text} />));

   beforeEach(()=> expect(screen.findByText({toLink , icon, text})).toBeInTheDocument());

});
