import { createServer } from "miragejs";

let skillsData = [
  { skillName: "HTML", skillRange: "100" },
  { skillName: "CSS", skillRange: "90" },
  { skillName: "JavaScript", skillRange: "85" },
  { skillName: "ReactJS", skillRange: "90" },
  { skillName: "Node.Js", skillRange: "80" },
  { skillName: "Express.Js", skillRange: "85" },
];

const educationsData = [
  {
    date: "2023",
    school: "Epam UpSkill",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2022",
    school: "IATC",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2013-2017",
    school: "AACU",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
];

if (localStorage.getItem("skillsData")) {
  skillsData = JSON.parse(localStorage.getItem("skillsData"));
}

export default function server() {
  createServer({
    routes() {
      this.namespace = "api";
      this.get(
        "/educations",
        () => {
          return educationsData;
        },
        { timing: 3000 }
      );

      this.post("/skills", (schema, request) => {
        const newSkill = JSON.parse(request.requestBody);
        skillsData.push(newSkill);
        localStorage.setItem("skillsData", JSON.stringify(skillsData));
        return skillsData;
      });

      this.get(
        "/skills",
        () => {
          console.log(skillsData);
          return skillsData;
        },
        { timing: 3000 }
      );
    },
  });
}
