
import { configureStore } from '@reduxjs/toolkit';

import { setupListeners } from '@reduxjs/toolkit/query';
import { eduSlice } from './api/eduSlice';
import { skillSlice } from './api/skillSlice';

const store = configureStore({
  reducer: {
    [skillSlice.reducerPath]: skillSlice.reducer,
    [eduSlice.reducerPath]: eduSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>

  getDefaultMiddleware().concat(skillSlice.middleware, eduSlice.middleware)  
});

setupListeners(store.dispatch);
export default store;
