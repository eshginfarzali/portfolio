import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const skillSlice = createApi({
  reducerPath: "skills",
  baseQuery: fetchBaseQuery({ baseUrl: "/api/" }),

  endpoints: (builder) => ({
    postSkills: builder.mutation({
      query: (data) => ({
        url: "skills",
        method: "POST",
        body: data,
        headers: {
          "Content-Type": "application/json",
        },
      }),
 
    }),
    getSkills: builder.query({
      query: () => "skills",
    }),
  }),
});

export const { useGetSkillsQuery, usePostSkillsMutation } = skillSlice;
