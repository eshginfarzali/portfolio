import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const eduSlice = createApi({
  reducerPath: 'educations',
  baseQuery: fetchBaseQuery({ baseUrl: '/api/' }),
  endpoints: (builder) => ({
    getEducations: builder.query({
      query: () => 'educations',
    }),
  }),
});

export const { useGetEducationsQuery } = eduSlice;
