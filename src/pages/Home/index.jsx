import styled, { keyframes } from 'styled-components';
import { Link } from 'react-router-dom';
import imageProfile from '../../assets/images/eshgin1.jpg';
import homeBackground from '../../assets/images/homebackground.jpg';
import profileBackground from '../../assets/images/2RNb.gif';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;
  background-image: url(${homeBackground});
  background-size: cover;
  background-size: no-repeat;
  background-clip: content-box;
  gap: 18px;
`;
const bounceBorder = keyframes`
  0%, 100% {
    transform: translateY(0);
    border: 4px solid transparent;
  }
  50% {
    transform: translateY(-20px);
    border: 4px solid rgb(213, 1, 255);
  }
`;
const Profile = styled.div`
  width: 200px;
  height: 200px;
  background-image: url(${profileBackground});
  background-size: 150%;
  background-position: center;
  padding: 8px;
  border: 4px solid rgb(20, 11, 22);
  animation: ${bounceBorder} 4s infinite linear;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;

  a > div > img {
    border-radius: 50%;
    width: 130px;
    height: 130px;
  }
`;



const H1 = styled.h1`
  font-size: 25px;
  font-weight: 800;
  color: #fff;
  letter-spacing: 4px;
`;

const H2 = styled.h2`
  font-size: 17px;
  font-weight: 400;
  color: #ece6e6;
  letter-spacing: 1px;
`;

const P = styled.p`
  max-width: 600px;
  font-size: 14px;
  font-weight: 300;
  color: #bcb5b5;
  text-align: center;
  line-height: 1.5;
`;

const Button = styled.button`
  width: 120px;
  max-height: 70px;
  font-size: 15px;
  text-transform: uppercase;
  letter-spacing: 1.3px;
  font-weight: 400;
  color: #fff;
  font-family: "Barlow Condensed", sans-serif;
  background: linear-gradient(90deg, rgb(228, 129, 230) 0%, rgb(255, 1, 234) 100%);
  border: none;
  border-radius: 20px;
  transition: all 0.3s ease-in-out 0s;
  cursor: pointer;
  outline: none;
  position: relative;
  padding: 10px;

  &:hover {
    background: linear-gradient(90deg, rgb(255, 1, 234) 0%, rgb(228, 129, 230) 100%);
  }
`;

export const Home = () => {
  return (
    <Container>
      <Profile>
        <Link to="./inner">
          <div>
            <img src={imageProfile} alt="eshgin" />
          </div>
        </Link>
      </Profile>
      <H1>Eshgin FARZALI</H1>
      <H2>MERN Stack Engineer</H2>
      <P>
        Experienced MERN stack engineer proficient in MongoDB, Express.js,
        React, and Node.js. Passionate about crafting dynamic web applications.
        Let`s build something amazing together!
      </P>
      <Button>
        <Link to="./inner">Know More</Link>
      </Button>
    </Container>
  );
};


