import styled from "styled-components";
import { Title } from "../../../components";

const P = styled.p`
  font-family: Open Sans;
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  letter-spacing: 0em;
  text-align: left;
  color: #222935;
  width: 95%;
`;

export const About = () => {
  return (
    <div id="about">
      <Title title="About me" />
      <P>
        I am a talented and driven Front-end Developer with a passion for
        creating visually stunning and user-friendly websites. With my strong
        background in HTML, CSS, JavaScript, ReactJs and other web technologies,
        I am able to develop interactive and engaging experiences for web users.
        I have a strong eye for design and a deep understanding of user
        experience, which I use to bring ideas to life in a way that is both
        beautiful and functional. My experience with both responsive design and
        cross-browser compatibility ensures that my websites are accessible on
        any device. I am constantly seeking new challenges and opportunities to
        expand my skills and knowledge in the ever-evolving world of web
        development. I am a quick learner and a team player, and I thrive in
        fast-paced and dynamic environments. If you are looking for a Front-end
        Developer with a proven track record of delivering high-quality work on
        time and within budget, look no further. I am ready to put my skills and
        expertise to work for you.
      </P>
    </div>
  );
};
