import {
  RangeSkill,
  SkillLine,
  Title,
  Loader,
  Modal,
} from "../../../components";
import { useGetSkillsQuery } from "../../../redux/api/skillSlice";

export const Skill = () => {
  const { data, error, isLoading, refetch} = useGetSkillsQuery();
  if (isLoading) {
    return <Loader />;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div id="skills">
      <Title title="Skills" />
      <Modal onRefetch={refetch} />
      {data &&
        data.map((skill, index) => (
          <RangeSkill
            key={index}
            text={skill.skillName}
            value={skill.skillRange}
          />
        ))}
      <SkillLine />
    </div>
  );
};
