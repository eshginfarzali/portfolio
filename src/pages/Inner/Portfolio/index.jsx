import { useState } from "react";
import styled from "styled-components";
import { PortfolioCard, Title } from "../../../components";
import ProjectOne from "../../../assets/images/project-1.png";
import ProjectTwo from "../../../assets/images/project-2.png";
import ProjectThree from "../../../assets/images/project-3.png";
import { Link} from "react-router-dom";

const LinkNav = styled.div`
  margin-bottom: 20px;
  color: var(--title-color);
  .active {
    color: var(--text-color);
  }
`;

const Container = styled.div`
  display: flex;
  justify-content: center;
  gap: 20px;
  flex-wrap: wrap;
`;

export const Portfolio = () => {
  const [selectedCategory, setSelectedCategory] = useState("All");

  const portfolioItems = [
    {
      image: ProjectOne,
      name: "FeedBack",
      desc: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, pede justo, fringilla vel, aliquet nec, vulputate eget, arcu...",
      link: "https://feedback-board-eshgin.vercel.app/",
      category: "Code",
    },
    {
      image: ProjectTwo,
      name: "DevLinks",
      desc: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, pede justo, fringilla vel, aliquet nec, vulputate eget, arcu...",
      link: "https://devlinks-eshginfarzali.vercel.app/",
      category: "Code",
    },
    {
      image: ProjectThree,
      name: "Vivo Hospital",
      desc: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, pede justo, fringilla vel, aliquet nec, vulputate eget, arcu...",
      link: "https://vivohospital.netlify.app/",
      category: "UI",
    },
   
  ];

  const filteredItems = selectedCategory === "All"
    ? portfolioItems
    : portfolioItems.filter(item => item.category === selectedCategory);

  return (
    <div id="portfolio">
      <Title title="Portfolio" />
      <LinkNav>
        <Link
          to="#"
          onClick={() => setSelectedCategory("All")}
          className={selectedCategory === "All" ? "active" : ""}
        >
          ALL
        </Link>
        
        <Link
          to="#"
          onClick={() => setSelectedCategory("Code")}
          className={selectedCategory === "Code" ? "active" : ""}
        >
         / CODE /
        </Link>
        
        <Link
          to="#"
          onClick={() => setSelectedCategory("UI")}
          className={selectedCategory === "UI" ? "active" : ""}
        >
          UI
        </Link>
      </LinkNav>
      <Container>
        {filteredItems.map((item, index) => (
          <PortfolioCard
            key={index}
            image={item.image}
            name={item.name}
            desc={item.desc}
            link={item.link}
          />
        ))}
     
      </Container>
    </div>
  );
};


