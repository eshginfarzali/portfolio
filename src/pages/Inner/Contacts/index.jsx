import { FaPhoneAlt, FaSkype, FaTwitter, FaFacebookF } from "react-icons/fa";
import { GrMail } from "react-icons/gr";
import { Address, Title } from '../../../components';

export const Contact = () => {
  const contactData = [
    {
      toLink: "tel:+994506472665",
      icon: <FaPhoneAlt />,
      text: "+994 50 647 26 65",
    },
    {
      toLink: "mailto:eshqinferzeliyev@gmail.com",
      icon: <GrMail />,
      text: "eshqinferzeliyev@gmail.com",
    },
    {
      toLink: "https://join.skype.com/invite/HvbEpWScOYg0",
      icon: <FaSkype />,
      text: "Skype",
    },
    {
      toLink: "https://www.facebook.com/eshqinf/",
      icon: <FaFacebookF />,
      text: "Facebook",
    },
    {
      toLink: "https://twitter.com/eshgnf",
      icon: <FaTwitter />,
      text: "Twitter",
    },
  ];

  return (
    <div id="contacts">
      <Title title="Contacts" />
      {contactData.map((contact, index) => (
        <Address key={index} toLink={contact.toLink} icon={contact.icon} text={contact.text} />
      ))}
    </div>
  );
};
