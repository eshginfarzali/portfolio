import styled from 'styled-components';
import { TimeLine, Title, Loader} from '../../../components';
import { useGetEducationsQuery } from '../../../redux/api/eduSlice';

const Container = styled.div`
  width: 100%;
  height: 50vh;
  overflow-y: scroll;
`;

export const Educations = () => {
  const { data, error, isLoading } = useGetEducationsQuery();

  if (isLoading) {
    return <Loader/>
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div id='educations'>
      <Title title='Educations' />
      <Container>
        {data && data.map((education, index) => (
          <TimeLine key={index} date={education.date} school={education.school} desc={education.desc} />
        ))}
      </Container>
    </div>
  );
};


