import { Title, Feedback } from "../../../components";
import Avatar from "../../../assets/images/user.jpg";

export const Feedbacks = () => {
  const feedbackData = [
    {
      text:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas dolorum numquam deleniti, omnis dignissimos consectetur voluptates ullam fuga eligendi dicta officiis recusandae harum dolorem soluta magni eum! Sequi consequatur, numquam rerum officiis dolor saepe deleniti quaerat, asperiores optio reiciendis vitae id natus quisquam perspiciatis beatae voluptatem. Magnam officia accusamus suscipit debitis totam? Tenetur ullam consequatur laboriosam aut natus tempore beatae possimus, fugit repudiandae ducimus, illo veritatis aliquid libero excepturi consequuntur obcaecati tempora, omnis assumenda quod dicta voluptatem. Numquam, quaerat velit! Beatae facilis sequi officia maxime, quod vero obcaecati? Corporis ducimus deserunt sunt quo delectus vel maiores veritatis harum nulla sint",
      image: Avatar,
      name: "Eshgin Farzaliyev - Front-end Developer,",
      link: "https://eshginfarzali.netlify.app/",
      siteName: "eshginfarzali.netlify.app",
    },
    {
      text:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas dolorum numquam deleniti, omnis dignissimos consectetur voluptates ullam fuga eligendi dicta officiis recusandae harum dolorem soluta magni eum! Sequi consequatur, numquam rerum officiis dolor saepe deleniti quaerat, asperiores optio reiciendis vitae id natus quisquam perspiciatis beatae voluptatem. Magnam officia accusamus suscipit debitis totam? Tenetur ullam consequatur laboriosam aut natus tempore beatae possimus, fugit repudiandae ducimus, illo veritatis aliquid libero excepturi consequuntur obcaecati tempora, omnis assumenda quod dicta voluptatem. Numquam, quaerat velit! Beatae facilis sequi officia maxime, quod vero obcaecati? Corporis ducimus deserunt sunt quo delectus vel maiores veritatis harum nulla sint",
      image: Avatar,
      name: "Eshgin Farzaliyev - Front-end Developer,",
      link: "https://eshginfarzali.netlify.app/",
      siteName: "eshginfarzali.netlify.app",
    },
  ];

  return (
    <div id="feedback">
      <Title title="Feedback" />
      {feedbackData.map((feedback, index) => (
        <Feedback
          key={index}
          text={feedback.text}
          image={feedback.image}
          name={feedback.name}
          link={feedback.link}
          siteName={feedback.siteName}
        />
      ))}
    </div>
  );
};
