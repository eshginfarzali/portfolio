import { Expertise, Title } from '../../../components';

export const Experience = () => {
  const experienceData = [
    {
      company: "Google",
      date: "2019-2022",
      job: "Front-end developer / PHP programmer",
      description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringil",
    },
    {
      company: "Twitter",
      date: "2017",
      job: "Web Developer",
      description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringil",
    },
  ];

  return (
    <div id="experience">
      <Title title="Experience" />
      {experienceData.map((experience, index) => (
        <Expertise
          key={index}
          company={experience.company}
          date={experience.date}
          job={experience.job}
          description={experience.description}
        />
      ))}
    </div>
  );
};
