import {  createBrowserRouter} from 'react-router-dom';

import {  Home, NotFound } from '../pages';
import Inner  from '../pages/Inner/Inner';  

export const router = createBrowserRouter([
  {
    index: true,
    element: <Home />,
  },

  {
    path: 'inner',
    element: <Inner/>,
  },
  {
    path: '*',
    element: <NotFound />,
  },
 
  
]);