import {BiLoader} from 'react-icons/bi'
import styled from 'styled-components'

const StyledLoading =styled.div`
width: 100%;
height: 30vh;
display: flex;
justify-content: center;
align-items: center;
    .loading-icon {
  color: teal;
  font-size: 3rem;
  animation: animate 2s infinite;
}

@keyframes animate {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(720deg);
  }
}
`

export const Loader = () => {
  return (
    <StyledLoading>
        <BiLoader className="loading-icon"/>
    </StyledLoading>
  )
}
