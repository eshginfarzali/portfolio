/*eslint-disable */

import styled from "styled-components";

const RangeBox = styled.div`
margin-bottom: 25px;
  height: 29px;
  background: #26c17e;
  display: flex;
  align-items: center;
  width: ${(props) => props.value + "%"};;
`;
const SkillName = styled.p`
  font-family: Open Sans;
  font-size: 16px;
  font-weight: 400;
  line-height: 19px;
  letter-spacing: 0em;
  text-align: left;
  color: #fff;
  padding-left: 15px;
`;

export const RangeSkill = ({ text, value }) => {
  return (
    <RangeBox value={value}>
      <SkillName>
        {text} {value}%
      </SkillName>
    </RangeBox>
  );
};
