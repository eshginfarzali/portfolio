/*eslint-disable */
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledComponent = styled.div`
  width: 308px;
  height: 190px;
  background: url(${(props) => props.image});
  background-size: cover;
  position: relative;
  overflow: hidden;
  &:hover {
    background-color: #fff;
  }

  & > .textBox {
    width: 308px;
    height: 190px;
    background: #ffffffee;
    border: 1px solid #ddd;
    position: absolute;
    top: 100%;
    left: 0;
    transition: top 0.3s ease;
    padding: 20px 20px 0 15px;
    display: flex;
    flex-direction: column;
    gap: 10px;
    h2 {
      font-family: Open Sans;
      font-size: 16px;
      font-weight: 700;
      line-height: 19px;
      letter-spacing: 0em;
      text-align: left;
      color: var(--title-color);
    }
    p {
      font-family: Open Sans;
      font-size: 14px;
      font-weight: 400;
      line-height: 21px;
      letter-spacing: 0em;
      text-align: left;
      color: var(--text-color);
    }
    a {
      font-family: Open Sans;
      font-size: 14px;
      font-weight: 400;
      line-height: 17px;
      letter-spacing: 0em;
      text-align: left;
      text-decoration: underline;
      color: var(--title-color);
    }
  }

  &:hover > .textBox {
    top: 0;
  }
`;

export const PortfolioCard = ({ image, name, desc, link }) => {
  return (
    <StyledComponent image={image}>
      <div className="textBox">
        <h2>{name}</h2>
        <p>{desc}</p>
        <Link to={link} target="_blank">
          View resource
        </Link>
      </div>
    </StyledComponent>
  );
};
