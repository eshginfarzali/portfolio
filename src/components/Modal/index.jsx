import { useEffect, useState } from "react";
import { FaEdit } from "react-icons/fa";
import styled from "styled-components";
import { useForm, Controller } from "react-hook-form";
import { usePostSkillsMutation } from "../../redux/api/skillSlice";
import PropTypes from 'prop-types';

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-bottom: 10px;

  button {
    border-radius: 8px;
    border: 1px solid gray;
    padding: 10px;
    background-color: ${(props) => (props.disabled ? "gray" : "green")};
    color: white;
    cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
  }
`;

const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  display: ${(props) => (props.open ? "block" : "none")};
  z-index: 1;
`;

const ModalContent = styled.div`
  background: white;
  width: 450px;
  margin: 100px auto;
  padding: 20px;
  border-radius: 8px;
`;

const InputContainer = styled.div`
  margin-bottom: 20px;

  input {
    border: 1px solid gray;
    padding: 10px;
    width: 90%;
    border-radius: 8px;
  }
  p {
    color: red;
    margin: 5px 0;
  }
`;

export const Modal = ({ onRefetch }) => {
  const [addSkill] = usePostSkillsMutation();

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const openModal = () => {
    reset();
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const onSubmit = async (data) => {
    console.log(data);
    await addSkill(data);
    onRefetch();
    closeModal();
  };

  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const handleCloseModal = (e) => {
      if (e.target === e.currentTarget) {
        closeModal();
      }
    };

    if (isOpen) {
      window.addEventListener("click", handleCloseModal);
    }

    return () => {
      window.removeEventListener("click", handleCloseModal);
    };
  }, [isOpen]);

  return (
    <Container disabled={!Object.keys(errors).length === 0}>
      <button onClick={openModal}>
        <FaEdit style={{ marginRight: "5px" }} />
        Open Edit
      </button>

      <ModalWrapper open={isOpen}>
        <ModalContent>
          <form onSubmit={handleSubmit(onSubmit)}>
            <InputContainer>
              <Controller
                name="skillName"
                control={control}
                defaultValue=""
                rules={{ required: "Skill name is a required field" }}
                render={({ field }) => (
                  <>
                    <input {...field} type="text" placeholder="Skill Name" />
                    {errors.skillName && <p>{errors.skillName.message}</p>}
                  </>
                )}
              />
            </InputContainer>

            <InputContainer>
              <Controller
                name="skillRange"
                control={control}
                defaultValue=""
                rules={{
                  required: "Skill range is a required field",
                  validate: {
                    isNumber: (value) =>
                      !isNaN(value) || "Skill range must be a number type",
                    minValue: (value) =>
                      parseFloat(value) >= 10 ||
                      "Skill range must be greater than or equal to 10",
                    maxValue: (value) =>
                      parseFloat(value) <= 100 ||
                      "Skill range must be less than or equal to 100",
                  },
                }}
                render={({ field }) => (
                  <>
                    <input {...field} type="text" placeholder="Skill Range" />
                    {errors.skillRange && <p>{errors.skillRange.message}</p>}
                  </>
                )}
              />
            </InputContainer>

            <button type="submit" disabled={!Object.keys(errors).length === 0}>
              Add Skill
            </button>
          </form>
        </ModalContent>
      </ModalWrapper>
    </Container>
  );
};

Modal.propTypes = {
  onRefetch: PropTypes.any
};